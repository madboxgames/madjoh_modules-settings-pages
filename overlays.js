define([
	'require',
	'displayers/overlays/msgbox',
	'displayers/overlays/spinner'
], 
function(require, MsgBox, Spinner){
	var Overlays = {};
	var overlaysList = [
		MsgBox,
		Spinner
	];
	for(var i = 0; i < overlaysList.length; i++){
		var overlayKey = overlaysList[i].prototype.name;
		Overlays[overlayKey] = overlaysList[i];
	}
	
	return Overlays;
});
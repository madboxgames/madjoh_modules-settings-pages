define([
	'require',
	'controllers/account',
	
	'controllers/challenges',
	'controllers/actualities',
	'controllers/friendship',
	'controllers/notifications',
	'controllers/coin_transactions'
], 
function(require, Account, Actualities, Challenges, Friendship, Notifications, CoinTransactions){
	var Controllers = {};
	var ControllersList = [
		Account,
		Challenges,
		Actualities,
		Friendship,
		Notifications,
		CoinTransactions
	];

	for(var i=0; i<ControllersList.length; i++) Controllers[ControllersList[i].prototype.name] = ControllersList[i];

	return Controllers;
});
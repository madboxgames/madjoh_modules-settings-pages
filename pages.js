define([
	'require',
	'displayers/pages/account/index',
	'displayers/pages/tutorial/index',
	'displayers/pages/main_container/index',
	'displayers/pages/challenges/index',
	'displayers/pages/coin_transactions/index'
], 
function(require, Account, Tutorial, MainContainer, Challenges, CoinTransactions){
	var Pages = {};
	var pageGroups = [
		Account,
		Tutorial,
		MainContainer,
		Challenges,
		CoinTransactions
	];
	for(var i = 0; i < pageGroups.length; i++){
		for(var j = 0; j < pageGroups[i].length; j++){
			var pageKey = pageGroups[i][j].prototype.name;
			Pages[pageKey] = pageGroups[i][j];
		}
	}
	
	return Pages;
});